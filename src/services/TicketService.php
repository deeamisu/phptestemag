<?php

namespace Src\Services;

use Src\Entities\Ticket;

class TicketService
{
    /**
     * @param array $tickets
     * @return array
     */
    public function sortTickets(array $tickets): array
    {
       /** @var Ticket[] $sortedTickets */
       $sortedTickets = [];
       $sortedTickets []= array_pop($tickets);

       while (count($tickets) > 0) {
           /** @var Ticket[] $tickets */
           foreach ($tickets as $key => $ticket) {
               if ($ticket->getArrival() == $sortedTickets[0]->getDeparture()) {
                   array_unshift($sortedTickets, $ticket);
                   unset($tickets[$key]);
               }
               if ($ticket->getDeparture() == $sortedTickets[count($sortedTickets) -1 ]->getArrival()) {
                   array_push($sortedTickets, $ticket);
                   unset($tickets[$key]);
               }
           }
       }

       return $sortedTickets;
    }

    /**
     * @param array $tickets
     * @return array
     */
    public function transformTicketsArrayFromObjectArrayToStringArray(array $tickets): array
    {
        $response = [];
        /** @var Ticket[] $tickets */
        foreach ($tickets as $ticket) {
            $response []= $ticket->toArray();
        }

        return $response;
    }

    /**
     * @param array $rawData
     * @return bool
     */
    public function validate(array $rawData): bool
    {
        if (!isset($rawData['transport'])) return false;
        if (!isset($rawData['departure'])) return false;
        if (!isset($rawData['arrival'])) return false;
        if (!isset($rawData['details'])) return false;

        return true;
    }
}