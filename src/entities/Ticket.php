<?php

namespace Src\Entities;

class Ticket
{

    private $transport;

    private $departure;

    private $arrival;

    private $details;

    /**
     * Ticket constructor.
     * @param string $transport
     * @param string $departure
     * @param string $arrival
     * @param string $details
     */
    public function __construct(
        string $transport,
        string $departure,
        string $arrival,
        string $details
    )
    {
        $this->transport = $transport;
        $this->departure = $departure;
        $this->arrival = $arrival;
        $this->details = $details;
    }

    /**
     * @return string|null
     */
    public function getDeparture(): ?string
    {
        return $this->departure;
    }

    /**
     * @return string|null
     */
    public function getArrival(): ?string
    {
        return $this->arrival;
    }

    public function toArray(): array
    {
        return [
            'departure' => $this->departure,
            'transport' => $this->transport,
            'arrival' => $this->arrival,
            'details' => $this->details
        ];
    }
}