<?php

namespace Src\Controller;

use Exception;
use Src\Entities\Ticket;
use Src\Services\TicketService;

class TicketController
{
    private $ticketService;

    /**
     * TicketController constructor.
     * @param TicketService $ticketService
     */
    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    /**
     * @param array $input
     */
    public function processRequest(array $input)
    {
        $tickets = [];
        foreach ($input as $rawData) {
            try {
                if (!$this->ticketService->validate($rawData)) {
                    throw new Exception('Missing fields');
                }
                $tickets []= new Ticket(
                    $rawData['transport'],
                    $rawData['departure'],
                    $rawData['arrival'],
                    $rawData['details']
                );
            } catch (Exception $exception) {
                header("400 Bad Request");
                http_response_code(400);
                echo json_encode(["message" => "Bad Request"]);
                exit();
            }
        }

        $sortedTickets = $this->ticketService->sortTickets($tickets);
        $response = $this->ticketService->transformTicketsArrayFromObjectArrayToStringArray($sortedTickets);

        http_response_code(200);
        echo json_encode($response);
    }
}