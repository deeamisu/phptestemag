<?php

require __DIR__."/../vendor/autoload.php";

use Src\Controller\TicketController;
use Src\Services\TicketService;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, OPTIONS");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode('/', $uri);

if (($uri[3] !== 'api') && ($uri[4] !== 'ticket')) {
    header("400 Bad Request");
    exit();
}

$method = $_SERVER['REQUEST_METHOD'];
if ($method != "POST") {
    header("400 Bad Request");
    exit();
}

$input = json_decode(file_get_contents('php://input'),true);

$controller = new TicketController(new TicketService());
$controller->processRequest($input);