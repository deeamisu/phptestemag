    In order to run the project , you have to follow these steps:

1. composer install
2. php -S localhost:80                  // in project folder, in order to start a PHP dev Server
3. vendor/bin/phpunit tests --testdox   // to run all tests, with pretty output (note that if the PHP server is not on, the endpoint test will fail)

4. visit http://localhost/public/web/dist/index.html    // SwagerDoc - here you can test the endpoint and required functionality