<?php

namespace Test;

require_once __DIR__."/../src/entities/Ticket.php";
require_once __DIR__."/../src/services/TicketService.php";
require_once __DIR__."/../vendor/guzzlehttp/guzzle/src/Client.php";

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use Src\Entities\Ticket;
use Src\Services\TicketService;

class TicketTest extends TestCase
{
    private static $expectedTickets;

    private static $ticketService;

    private static $requestData;

    public function setUp(): void
    {
        parent::setUp();
        self::$expectedTickets = [
            new Ticket("car", "A","B","private"),
            new Ticket("car", "B","C","private"),
            new Ticket("car", "C","D","private"),
            new Ticket("car", "D","E","private")
        ];
        self::$ticketService = new TicketService();
        self::$requestData = [
            ["transport" => "car", "departure" => "C", "arrival" => "D", "details" => "private"],
            ["transport" => "car", "departure" => "B", "arrival" => "C", "details" => "private"],
            ["transport" => "car", "departure" => "D", "arrival" => "E", "details" => "private"],
            ["transport" => "car", "departure" => "A", "arrival" => "B", "details" => "private"],
        ];
    }

    public function testSortTicketsSuccess()
    {
        $tickets = [
            new Ticket("car", "C","D","private"),
            new Ticket("car", "B","C","private"),
            new Ticket("car", "D","E","private"),
            new Ticket("car", "A","B","private")
        ];

        $sortedTickets = self::$ticketService->sortTickets($tickets);

        self::assertEquals(self::$expectedTickets, $sortedTickets, "Tickets sorted successfully!");
    }

    public function testTransformTicketsArrayFromObjectArrayToStringArray()
    {
        $tickets = [
            ["departure" => "A", "transport" => "car", "arrival" => "B", "details" => "private"],
            ["departure" => "B", "transport" => "car", "arrival" => "C", "details" => "private"],
            ["departure" => "C", "transport" => "car", "arrival" => "D", "details" => "private"],
            ["departure" => "D", "transport" => "car", "arrival" => "E", "details" => "private"],
        ];

        $transformedTickets = self::$ticketService
            ->transformTicketsArrayFromObjectArrayToStringArray(self::$expectedTickets);

        self::assertEquals($tickets, $transformedTickets);
    }

    public function testValidateRequestData()
    {
        $validArray = [
            "transport" => "plane",
            "departure" => "Stalingrad",
            "arrival" => "Zurich",
            "details" => "KLM Airlines"
        ];
        self::assertTrue(self::$ticketService->validate($validArray));

        array_pop($validArray);
        self::assertNotTrue(self::$ticketService->validate($validArray));
    }

    /**
     * @throws GuzzleException
     */
    public function testPostEndpoint()
    {
        $client = new Client([
            'base_uri'        => 'http://localhost:80',
            'timeout'         => 10,
            'allow_redirects' => false,
        ]);

        $response = $client->request('POST', 'public/index.php/api/ticket', [
            'json' => self::$requestData
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $expectedData = [
            ["departure" => "A", "transport" => "car", "arrival" => "B", "details" => "private"],
            ["departure" => "B", "transport" => "car", "arrival" => "C", "details" => "private"],
            ["departure" => "C", "transport" => "car", "arrival" => "D", "details" => "private"],
            ["departure" => "D", "transport" => "car", "arrival" => "E", "details" => "private"],
        ];
        $data = json_decode($response->getBody(), true);
        $this->assertEquals($expectedData, $data);
    }

}